import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

class Main {
  public static void main(String[] args) {
    double pR = 0.12; //probab of rejection
    double pN = 1.0 - pR; //the contrary
    //no more than 2 rejects out of 10:
    double p2 = Math.pow(pN,10)+10.0*Math.pow(pN,9)*pR+45.0*Math.pow(pN,8)*Math.pow(pR,2);
    p2 = Math.round(1000.0*p2)/1000.0;
    //at least 2
    double pc = 1.0-Math.pow(pN,10)-10.0*Math.pow(pN,9)*pR;
    pc = Math.round(1000.0*pc)/1000.0;

    System.out.println(pc);
  }
}
